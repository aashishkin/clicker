﻿namespace WindowsFormsApplication3
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.buttonPlus1 = new System.Windows.Forms.Button();
            this.timerPlus1 = new System.Windows.Forms.Timer(this.components);
            this.buttonPlus10 = new System.Windows.Forms.Button();
            this.buttonAutoPlus1 = new System.Windows.Forms.Button();
            this.buttonAutoPlus10 = new System.Windows.Forms.Button();
            this.labelResult = new System.Windows.Forms.Label();
            this.timerPlus10 = new System.Windows.Forms.Timer(this.components);
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.buttonUpgrade = new System.Windows.Forms.Button();
            this.buttonPlus100 = new System.Windows.Forms.Button();
            this.buttonAuto100 = new System.Windows.Forms.Button();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.buttonPlus333 = new System.Windows.Forms.Button();
            this.buttonAuto333 = new System.Windows.Forms.Button();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.buttonPlus666 = new System.Windows.Forms.Button();
            this.buttonAuto666 = new System.Windows.Forms.Button();
            this.checkBox5 = new System.Windows.Forms.CheckBox();
            this.buttonPlus1500 = new System.Windows.Forms.Button();
            this.buttonAuto1500 = new System.Windows.Forms.Button();
            this.checkBox6 = new System.Windows.Forms.CheckBox();
            this.buttonPlus3000 = new System.Windows.Forms.Button();
            this.buttonAuto3000 = new System.Windows.Forms.Button();
            this.checkBox7 = new System.Windows.Forms.CheckBox();
            this.buttonPlus10000 = new System.Windows.Forms.Button();
            this.buttonAuto10000 = new System.Windows.Forms.Button();
            this.checkBox8 = new System.Windows.Forms.CheckBox();
            this.timerPlus100 = new System.Windows.Forms.Timer(this.components);
            this.timerPlus333 = new System.Windows.Forms.Timer(this.components);
            this.timerPlus666 = new System.Windows.Forms.Timer(this.components);
            this.timerPlus1500 = new System.Windows.Forms.Timer(this.components);
            this.timerPlus3000 = new System.Windows.Forms.Timer(this.components);
            this.timerPlus10000 = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // buttonPlus1
            // 
            this.buttonPlus1.Location = new System.Drawing.Point(12, 38);
            this.buttonPlus1.Name = "buttonPlus1";
            this.buttonPlus1.Size = new System.Drawing.Size(124, 23);
            this.buttonPlus1.TabIndex = 0;
            this.buttonPlus1.Text = "+1";
            this.buttonPlus1.UseVisualStyleBackColor = true;
            this.buttonPlus1.Click += new System.EventHandler(this.buttonPlus1_Click);
            // 
            // timerPlus1
            // 
            this.timerPlus1.Interval = 500;
            this.timerPlus1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // buttonPlus10
            // 
            this.buttonPlus10.Location = new System.Drawing.Point(12, 67);
            this.buttonPlus10.Name = "buttonPlus10";
            this.buttonPlus10.Size = new System.Drawing.Size(124, 23);
            this.buttonPlus10.TabIndex = 1;
            this.buttonPlus10.Text = "+10";
            this.buttonPlus10.UseVisualStyleBackColor = true;
            this.buttonPlus10.Visible = false;
            // 
            // buttonAutoPlus1
            // 
            this.buttonAutoPlus1.Enabled = false;
            this.buttonAutoPlus1.Location = new System.Drawing.Point(143, 38);
            this.buttonAutoPlus1.Name = "buttonAutoPlus1";
            this.buttonAutoPlus1.Size = new System.Drawing.Size(114, 23);
            this.buttonAutoPlus1.TabIndex = 3;
            this.buttonAutoPlus1.Text = "+1/sec";
            this.buttonAutoPlus1.UseVisualStyleBackColor = true;
            this.buttonAutoPlus1.Click += new System.EventHandler(this.buttonAutoPlus1_Click);
            // 
            // buttonAutoPlus10
            // 
            this.buttonAutoPlus10.Location = new System.Drawing.Point(143, 67);
            this.buttonAutoPlus10.Name = "buttonAutoPlus10";
            this.buttonAutoPlus10.Size = new System.Drawing.Size(114, 23);
            this.buttonAutoPlus10.TabIndex = 3;
            this.buttonAutoPlus10.Text = "+10/sec";
            this.buttonAutoPlus10.UseVisualStyleBackColor = true;
            this.buttonAutoPlus10.Visible = false;
            this.buttonAutoPlus10.Click += new System.EventHandler(this.buttonAutoPlus10_Click);
            // 
            // labelResult
            // 
            this.labelResult.AutoSize = true;
            this.labelResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelResult.Location = new System.Drawing.Point(12, 9);
            this.labelResult.Name = "labelResult";
            this.labelResult.Size = new System.Drawing.Size(24, 25);
            this.labelResult.TabIndex = 4;
            this.labelResult.Text = "0";
            this.labelResult.TextChanged += new System.EventHandler(this.labelResult_TextChanged);
            // 
            // timerPlus10
            // 
            this.timerPlus10.Interval = 500;
            this.timerPlus10.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Enabled = false;
            this.checkBox1.Location = new System.Drawing.Point(263, 43);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(15, 14);
            this.checkBox1.TabIndex = 5;
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Enabled = false;
            this.checkBox2.Location = new System.Drawing.Point(263, 71);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(15, 14);
            this.checkBox2.TabIndex = 5;
            this.checkBox2.UseVisualStyleBackColor = true;
            this.checkBox2.Visible = false;
            // 
            // buttonUpgrade
            // 
            this.buttonUpgrade.Enabled = false;
            this.buttonUpgrade.Location = new System.Drawing.Point(285, 38);
            this.buttonUpgrade.Name = "buttonUpgrade";
            this.buttonUpgrade.Size = new System.Drawing.Size(126, 226);
            this.buttonUpgrade.TabIndex = 6;
            this.buttonUpgrade.Text = "Уличшить кликер";
            this.buttonUpgrade.UseVisualStyleBackColor = true;
            this.buttonUpgrade.Click += new System.EventHandler(this.buttonUpgrade_Click);
            // 
            // buttonPlus100
            // 
            this.buttonPlus100.Location = new System.Drawing.Point(12, 96);
            this.buttonPlus100.Name = "buttonPlus100";
            this.buttonPlus100.Size = new System.Drawing.Size(124, 23);
            this.buttonPlus100.TabIndex = 1;
            this.buttonPlus100.Text = "+100";
            this.buttonPlus100.UseVisualStyleBackColor = true;
            this.buttonPlus100.Visible = false;
            this.buttonPlus100.Click += new System.EventHandler(this.buttonPlus100_Click);
            // 
            // buttonAuto100
            // 
            this.buttonAuto100.Location = new System.Drawing.Point(143, 96);
            this.buttonAuto100.Name = "buttonAuto100";
            this.buttonAuto100.Size = new System.Drawing.Size(114, 23);
            this.buttonAuto100.TabIndex = 3;
            this.buttonAuto100.Text = "+100/sec";
            this.buttonAuto100.UseVisualStyleBackColor = true;
            this.buttonAuto100.Visible = false;
            this.buttonAuto100.Click += new System.EventHandler(this.buttonAutoPlus100_Click);
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Enabled = false;
            this.checkBox3.Location = new System.Drawing.Point(263, 100);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(15, 14);
            this.checkBox3.TabIndex = 5;
            this.checkBox3.UseVisualStyleBackColor = true;
            this.checkBox3.Visible = false;
            // 
            // buttonPlus333
            // 
            this.buttonPlus333.Location = new System.Drawing.Point(12, 125);
            this.buttonPlus333.Name = "buttonPlus333";
            this.buttonPlus333.Size = new System.Drawing.Size(124, 23);
            this.buttonPlus333.TabIndex = 1;
            this.buttonPlus333.Text = "+333";
            this.buttonPlus333.UseVisualStyleBackColor = true;
            this.buttonPlus333.Visible = false;
            this.buttonPlus333.Click += new System.EventHandler(this.buttonPlus333_Click);
            // 
            // buttonAuto333
            // 
            this.buttonAuto333.Location = new System.Drawing.Point(143, 125);
            this.buttonAuto333.Name = "buttonAuto333";
            this.buttonAuto333.Size = new System.Drawing.Size(114, 23);
            this.buttonAuto333.TabIndex = 3;
            this.buttonAuto333.Text = "+333/sec";
            this.buttonAuto333.UseVisualStyleBackColor = true;
            this.buttonAuto333.Visible = false;
            this.buttonAuto333.Click += new System.EventHandler(this.buttonAuto333_Click);
            // 
            // checkBox4
            // 
            this.checkBox4.AutoSize = true;
            this.checkBox4.Enabled = false;
            this.checkBox4.Location = new System.Drawing.Point(263, 129);
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new System.Drawing.Size(15, 14);
            this.checkBox4.TabIndex = 5;
            this.checkBox4.UseVisualStyleBackColor = true;
            this.checkBox4.Visible = false;
            // 
            // buttonPlus666
            // 
            this.buttonPlus666.Location = new System.Drawing.Point(12, 154);
            this.buttonPlus666.Name = "buttonPlus666";
            this.buttonPlus666.Size = new System.Drawing.Size(124, 23);
            this.buttonPlus666.TabIndex = 1;
            this.buttonPlus666.Text = "+666";
            this.buttonPlus666.UseVisualStyleBackColor = true;
            this.buttonPlus666.Visible = false;
            this.buttonPlus666.Click += new System.EventHandler(this.buttonPlus666_Click);
            // 
            // buttonAuto666
            // 
            this.buttonAuto666.Location = new System.Drawing.Point(143, 154);
            this.buttonAuto666.Name = "buttonAuto666";
            this.buttonAuto666.Size = new System.Drawing.Size(114, 23);
            this.buttonAuto666.TabIndex = 3;
            this.buttonAuto666.Text = "+666/sec";
            this.buttonAuto666.UseVisualStyleBackColor = true;
            this.buttonAuto666.Visible = false;
            this.buttonAuto666.Click += new System.EventHandler(this.buttonAuto666_Click);
            // 
            // checkBox5
            // 
            this.checkBox5.AutoSize = true;
            this.checkBox5.Enabled = false;
            this.checkBox5.Location = new System.Drawing.Point(263, 158);
            this.checkBox5.Name = "checkBox5";
            this.checkBox5.Size = new System.Drawing.Size(15, 14);
            this.checkBox5.TabIndex = 5;
            this.checkBox5.UseVisualStyleBackColor = true;
            this.checkBox5.Visible = false;
            // 
            // buttonPlus1500
            // 
            this.buttonPlus1500.Location = new System.Drawing.Point(12, 183);
            this.buttonPlus1500.Name = "buttonPlus1500";
            this.buttonPlus1500.Size = new System.Drawing.Size(124, 23);
            this.buttonPlus1500.TabIndex = 1;
            this.buttonPlus1500.Text = "+1500";
            this.buttonPlus1500.UseVisualStyleBackColor = true;
            this.buttonPlus1500.Visible = false;
            this.buttonPlus1500.Click += new System.EventHandler(this.buttonPlus1500_Click);
            // 
            // buttonAuto1500
            // 
            this.buttonAuto1500.Location = new System.Drawing.Point(143, 183);
            this.buttonAuto1500.Name = "buttonAuto1500";
            this.buttonAuto1500.Size = new System.Drawing.Size(114, 23);
            this.buttonAuto1500.TabIndex = 3;
            this.buttonAuto1500.Text = "+1500/sec";
            this.buttonAuto1500.UseVisualStyleBackColor = true;
            this.buttonAuto1500.Visible = false;
            this.buttonAuto1500.Click += new System.EventHandler(this.buttonAuto1500_Click);
            // 
            // checkBox6
            // 
            this.checkBox6.AutoSize = true;
            this.checkBox6.Enabled = false;
            this.checkBox6.Location = new System.Drawing.Point(263, 187);
            this.checkBox6.Name = "checkBox6";
            this.checkBox6.Size = new System.Drawing.Size(15, 14);
            this.checkBox6.TabIndex = 5;
            this.checkBox6.UseVisualStyleBackColor = true;
            this.checkBox6.Visible = false;
            // 
            // buttonPlus3000
            // 
            this.buttonPlus3000.Location = new System.Drawing.Point(12, 212);
            this.buttonPlus3000.Name = "buttonPlus3000";
            this.buttonPlus3000.Size = new System.Drawing.Size(124, 23);
            this.buttonPlus3000.TabIndex = 1;
            this.buttonPlus3000.Text = "+3000";
            this.buttonPlus3000.UseVisualStyleBackColor = true;
            this.buttonPlus3000.Visible = false;
            this.buttonPlus3000.Click += new System.EventHandler(this.buttonPlus3000_Click);
            // 
            // buttonAuto3000
            // 
            this.buttonAuto3000.Location = new System.Drawing.Point(143, 212);
            this.buttonAuto3000.Name = "buttonAuto3000";
            this.buttonAuto3000.Size = new System.Drawing.Size(114, 23);
            this.buttonAuto3000.TabIndex = 3;
            this.buttonAuto3000.Text = "+3000/sec";
            this.buttonAuto3000.UseVisualStyleBackColor = true;
            this.buttonAuto3000.Visible = false;
            this.buttonAuto3000.Click += new System.EventHandler(this.buttonAuto3000_Click);
            // 
            // checkBox7
            // 
            this.checkBox7.AutoSize = true;
            this.checkBox7.Enabled = false;
            this.checkBox7.Location = new System.Drawing.Point(263, 216);
            this.checkBox7.Name = "checkBox7";
            this.checkBox7.Size = new System.Drawing.Size(15, 14);
            this.checkBox7.TabIndex = 5;
            this.checkBox7.UseVisualStyleBackColor = true;
            this.checkBox7.Visible = false;
            // 
            // buttonPlus10000
            // 
            this.buttonPlus10000.Location = new System.Drawing.Point(12, 241);
            this.buttonPlus10000.Name = "buttonPlus10000";
            this.buttonPlus10000.Size = new System.Drawing.Size(124, 23);
            this.buttonPlus10000.TabIndex = 1;
            this.buttonPlus10000.Text = "+10000";
            this.buttonPlus10000.UseVisualStyleBackColor = true;
            this.buttonPlus10000.Visible = false;
            this.buttonPlus10000.Click += new System.EventHandler(this.buttonPlus10000_Click);
            // 
            // buttonAuto10000
            // 
            this.buttonAuto10000.Location = new System.Drawing.Point(143, 241);
            this.buttonAuto10000.Name = "buttonAuto10000";
            this.buttonAuto10000.Size = new System.Drawing.Size(114, 23);
            this.buttonAuto10000.TabIndex = 3;
            this.buttonAuto10000.Text = "+10000/sec";
            this.buttonAuto10000.UseVisualStyleBackColor = true;
            this.buttonAuto10000.Visible = false;
            this.buttonAuto10000.Click += new System.EventHandler(this.buttonAuto10000_Click);
            // 
            // checkBox8
            // 
            this.checkBox8.AutoSize = true;
            this.checkBox8.Enabled = false;
            this.checkBox8.Location = new System.Drawing.Point(263, 245);
            this.checkBox8.Name = "checkBox8";
            this.checkBox8.Size = new System.Drawing.Size(15, 14);
            this.checkBox8.TabIndex = 5;
            this.checkBox8.UseVisualStyleBackColor = true;
            this.checkBox8.Visible = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(423, 273);
            this.Controls.Add(this.buttonUpgrade);
            this.Controls.Add(this.checkBox8);
            this.Controls.Add(this.checkBox7);
            this.Controls.Add(this.checkBox6);
            this.Controls.Add(this.checkBox5);
            this.Controls.Add(this.checkBox4);
            this.Controls.Add(this.checkBox3);
            this.Controls.Add(this.checkBox2);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.labelResult);
            this.Controls.Add(this.buttonAuto10000);
            this.Controls.Add(this.buttonAuto3000);
            this.Controls.Add(this.buttonAuto1500);
            this.Controls.Add(this.buttonAuto666);
            this.Controls.Add(this.buttonAuto333);
            this.Controls.Add(this.buttonAuto100);
            this.Controls.Add(this.buttonAutoPlus10);
            this.Controls.Add(this.buttonAutoPlus1);
            this.Controls.Add(this.buttonPlus10000);
            this.Controls.Add(this.buttonPlus3000);
            this.Controls.Add(this.buttonPlus1500);
            this.Controls.Add(this.buttonPlus666);
            this.Controls.Add(this.buttonPlus333);
            this.Controls.Add(this.buttonPlus100);
            this.Controls.Add(this.buttonPlus10);
            this.Controls.Add(this.buttonPlus1);
            this.Name = "Form1";
            this.Text = "Кликер";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonPlus1;
        private System.Windows.Forms.Timer timerPlus1;
        private System.Windows.Forms.Button buttonPlus10;
        private System.Windows.Forms.Button buttonAutoPlus1;
        private System.Windows.Forms.Button buttonAutoPlus10;
        private System.Windows.Forms.Label labelResult;
        private System.Windows.Forms.Timer timerPlus10;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.Button buttonUpgrade;
        private System.Windows.Forms.Button buttonPlus100;
        private System.Windows.Forms.Button buttonAuto100;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.Button buttonPlus333;
        private System.Windows.Forms.Button buttonAuto333;
        private System.Windows.Forms.CheckBox checkBox4;
        private System.Windows.Forms.Button buttonPlus666;
        private System.Windows.Forms.Button buttonAuto666;
        private System.Windows.Forms.CheckBox checkBox5;
        private System.Windows.Forms.Button buttonPlus1500;
        private System.Windows.Forms.Button buttonAuto1500;
        private System.Windows.Forms.CheckBox checkBox6;
        private System.Windows.Forms.Button buttonPlus3000;
        private System.Windows.Forms.Button buttonAuto3000;
        private System.Windows.Forms.CheckBox checkBox7;
        private System.Windows.Forms.Button buttonPlus10000;
        private System.Windows.Forms.Button buttonAuto10000;
        private System.Windows.Forms.CheckBox checkBox8;
        private System.Windows.Forms.Timer timerPlus100;
        private System.Windows.Forms.Timer timerPlus333;
        private System.Windows.Forms.Timer timerPlus666;
        private System.Windows.Forms.Timer timerPlus1500;
        private System.Windows.Forms.Timer timerPlus3000;
        private System.Windows.Forms.Timer timerPlus10000;
    }
}

