﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication3
{
    public partial class Form1 : Form
    {
        public int a = 0;
        public int upgradePrice = 100;
        public int clickerLVL = 1;

        public Form1()
        {
            InitializeComponent();
        }

        private void buttonPlus1_Click(object sender, EventArgs e)
        {
            a++;
            labelResult.Text = a.ToString();
        }


        private void labelResult_TextChanged(object sender, EventArgs e)
        {
            if (a >= 300)
            {
                buttonAutoPlus1.Enabled = true;
            }

            if (a >= 777 && buttonAutoPlus10.Visible)
            {
                buttonAutoPlus10.Enabled = true;
            }

            if (a < upgradePrice)
            {
                buttonUpgrade.Enabled = false;
            }
            else
            {
                buttonUpgrade.Enabled = true;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            a++;
            labelResult.Text = a.ToString();
        }

        private void buttonAutoPlus1_Click(object sender, EventArgs e)
        {
            if (timerPlus1.Enabled)
            {
                timerPlus1.Enabled = false;
                checkBox1.Checked = false;
            }
            else
            {
                timerPlus1.Enabled = true;
                checkBox1.Checked = true;
            }
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            a = a + 10;
            labelResult.Text = a.ToString();
        }

        private void buttonAutoPlus10_Click(object sender, EventArgs e)
        {
            if (timerPlus10.Enabled)
            {
                timerPlus10.Enabled = false;
                checkBox2.Checked = false;
            }
            else
            {
                timerPlus10.Enabled = true;
                checkBox2.Checked = true;
            }
        }

        private void buttonUpgrade_Click(object sender, EventArgs e)
        {
            a = a - upgradePrice;
            upgradePrice *= 5;
            clickerLVL++;
            switch (clickerLVL)
            {
                case 1:
                    buttonPlus10.Visible = true;
                    buttonAutoPlus1.Enabled = true;
                    break;
                case 2:
                    buttonPlus100.Visible = true;
                    buttonAutoPlus10.Enabled = true;
                    break;
                case 3:
                    buttonPlus333.Visible = true;
                    buttonAuto100.Enabled = true;
                    break;
                case 4:
                    buttonPlus666.Visible = true;
                    buttonAuto333.Enabled = true;
                    break;
                case 5:
                    buttonPlus1500.Visible = true;
                    buttonAuto666.Enabled = true;
                    break;
                case 6:
                    buttonPlus3000.Visible = true;
                    buttonAuto1500.Enabled = true;
                    break;
                case 7:
                    buttonPlus10000.Visible = true;
                    buttonAuto3000.Enabled = true;
                    break;
                case 8:
                    buttonAuto10000.Enabled = true;
                    break;
            }
        }

        private void buttonPlus100_Click(object sender, EventArgs e)
        {
            a = a + 100;
            labelResult.Text = a.ToString();
        }

        private void buttonPlus333_Click(object sender, EventArgs e)
        {
            a = a + 333;
            labelResult.Text = a.ToString();
        }

        private void buttonPlus666_Click(object sender, EventArgs e)
        {
            a = a + 666;
            labelResult.Text = a.ToString();
        }

        private void buttonPlus1500_Click(object sender, EventArgs e)
        {
            a = a + 1500;
            labelResult.Text = a.ToString();
        }

        private void buttonPlus3000_Click(object sender, EventArgs e)
        {
            a = a + 3000;
            labelResult.Text = a.ToString();
        }

        private void buttonPlus10000_Click(object sender, EventArgs e)
        {
            a = a + 10000;
            labelResult.Text = a.ToString();
        }

        private void buttonAutoPlus100_Click(object sender, EventArgs e)
        {
            if (timerPlus100.Enabled)
            {
                timerPlus100.Enabled = false;
                checkBox3.Checked = false;
            }
            else
            {
                timerPlus100.Enabled = true;
                checkBox3.Checked = true;
            }
        }

        private void buttonAuto333_Click(object sender, EventArgs e)
        {
            if (timerPlus333.Enabled)
            {
                timerPlus333.Enabled = false;
                checkBox4.Checked = false;
            }
            else
            {
                timerPlus333.Enabled = true;
                checkBox4.Checked = true;
            }
        }

        private void buttonAuto666_Click(object sender, EventArgs e)
        {
            if (timerPlus666.Enabled)
            {
                timerPlus666.Enabled = false;
                checkBox5.Checked = false;
            }
            else
            {
                timerPlus666.Enabled = true;
                checkBox5.Checked = true;
            }
        }

        private void buttonAuto1500_Click(object sender, EventArgs e)
        {
            if (timerPlus1500.Enabled)
            {
                timerPlus1500.Enabled = false;
                checkBox6.Checked = false;
            }
            else
            {
                timerPlus1500.Enabled = true;
                checkBox6.Checked = true;
            }
        }

        private void buttonAuto3000_Click(object sender, EventArgs e)
        {
            if (timerPlus3000.Enabled)
            {
                timerPlus3000.Enabled = false;
                checkBox7.Checked = false;
            }
            else
            {
                timerPlus3000.Enabled = true;
                checkBox7.Checked = true;
            }
        }

        private void buttonAuto10000_Click(object sender, EventArgs e)
        {
            if (timerPlus10000.Enabled)
            {
                timerPlus10000.Enabled = false;
                checkBox8.Checked = false;
            }
            else
            {
                timerPlus10000.Enabled = true;
                checkBox8.Checked = true;
            }
        }
    }
}
